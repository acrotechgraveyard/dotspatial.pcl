﻿using System;

namespace DotSpatial.Positioning
{
    public static partial class PortableCompatibilityExtensions
    {
        public static string ToUpper(this string source, System.Globalization.CultureInfo culture)
        {
#if PCL
            return source.ToUpper();
#else
            return source.ToUpper(culture);
#endif
        }

        public static string ToLower(this string source, System.Globalization.CultureInfo culture)
        {
#if PCL
            return source.ToLower();
#else
            return source.ToLower(culture);
#endif
        }

        public static string ToString(this char source, System.Globalization.CultureInfo culture)
        {
#if PCL
            return source.ToString();
#else
            return source.ToString(culture);
#endif
        }
    }

    #region Compat Classes

    public static class StringCompat
    {
        public static int Compare(string strA, string strB, bool ignoreCase, System.Globalization.CultureInfo culture)
        {
#if PCL
            if (culture == null)
            {
                throw new ArgumentNullException("culture");
            }
            if (ignoreCase)
            {
                return culture.CompareInfo.Compare(strA, strB, System.Globalization.CompareOptions.IgnoreCase);
            }
            return culture.CompareInfo.Compare(strA, strB, System.Globalization.CompareOptions.None);
#else
            return String.Compare(strA, strB, ignoreCase, culture);
#endif
        }
    }

    public static class MathCompat
    {
        public static double Truncate(double d)
        {
#if PCL
            return d < 0 ? Math.Ceiling(d) : Math.Floor(d);
#else
            return Math.Truncate(d);
#endif
        }
    }

    public static class ThreadCompat
    {
        public static void Sleep(int millisecondsTimeout)
        {
            
#if PCL
            new System.Threading.ManualResetEvent(false).WaitOne(millisecondsTimeout);
#else
            System.Threading.Thread.Sleep(millisecondsTimeout);
#endif
        }
    }

    public static class ExceptionCompat
    {
        public static ArgumentException CreateArgumentException(string message, string paramName, Exception innerException)
        {
#if PCL
            return new ArgumentException(message, innerException);
#else
            return new ArgumentException(message, paramName, innerException);
#endif
        }

        public static ArgumentOutOfRangeException CreateArgumentOutOfRangeException(string paramName, object actualValue, string message)
        {
#if PCL
            return new ArgumentOutOfRangeException(message);
#else
            return new ArgumentOutOfRangeException(paramName, actualValue, message);
#endif
        }
    }

    #endregion

#if PCL

    public enum MatrixOrder
    {
        Append,
        Prepend,
    }

    public class ApplicationException : Exception
    {
        public ApplicationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    #region Dummy Classes

    internal class TypeConverterAttribute : Attribute
    {
        public TypeConverterAttribute(string _)
        {
        }

        public TypeConverterAttribute(Type _)
        {
        }
    }

    internal class ExpandableObjectConverter
    {
    }

    internal class DefaultPropertyAttribute : Attribute
    {
        public DefaultPropertyAttribute(string _)
        {
        }
    }

    internal class CategoryAttribute : Attribute
    {
        public CategoryAttribute(string _)
        {
        }
    }

    internal class DescriptionAttribute : Attribute
    {
        public DescriptionAttribute(string _)
        {
        }
    }

    internal class BrowsableAttribute : Attribute
    {
        public BrowsableAttribute(bool _)
        {
        }
    }

    internal class ParenthesizePropertyNameAttribute : Attribute
    {
        public ParenthesizePropertyNameAttribute(bool _)
        {
        }
    }

    #endregion

#endif
}
