SET MSBUILD="C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"

%MSBUILD% DotSpatial.Positioning.PCL.csproj /t:Rebuild /p:Configuration=Release /p:Platform=AnyCPU /p:SolutionDir=..

DEL *.nupkg

..\.nuget\NuGet.exe Pack DotSpatial.Positioning.PCL.csproj -Prop Configuration=Release

PAUSE
