﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace DotSpatial.Projections
{
    public static partial class CompatibilityExtensions
    {
        public static string GetLocation(this Assembly source)
        {
#if PocketPC
            return source.GetName().CodeBase;
#else
            return source.Location;
#endif
        }

        public static bool EnumTryParse<T>(this string source, bool ignoreCase, out T result)
            where T : struct
        {
#if PocketPC
            var success = false;
            result = default(T);

            try
            {
                result = (T)Enum.Parse(typeof(T), source, ignoreCase);

                success = true;
            }
            catch { }

            return success;
#else
            return Enum.TryParse(source, ignoreCase, out result);
#endif
        }
    }
}
