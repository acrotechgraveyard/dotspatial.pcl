# DotSpatial

For more information see
https://dotspatial.codeplex.com/
For documentation and code samples see
https://dotspatial.codeplex.com/documentation
For discussions, questions and suggestions use
https://dotspatial.codeplex.com/discussions

Some sample projects there are at Examples folder.
Use DemoMap.exe to see basic functionality of DotSpatial library.

## PCL Port

This repository contains the Portable Class Library (PCL) port built for profile 344 (also known as the universally portable profile) for some of the DotSpatial components. This repository will also contain various tweaks and fixes to the ported components and publish these PCL components to NuGet. Currently the following components have been ported to a PCL library:
* DotSpatial.Positioning

## Subversion GitHub Mirror

This repository makes use of the subversion mirror set up at https://github.com/patsissons/DotSpatial
This mirror allows all subversion commits made to the original dotspatial repostory to be merged into this PCL port. The svn-mirror branch will always hold the most recent commit to the codeplex repository (or as recent as I can keep up with it). The end result is that all subversion commits are integrated into the commit history of this PCL port.

### How to Sync with Codeplex Subversion

These commands will allow developers to create their own subversion mirrored repository if that is desired. 

It should be noted that I was unable to complete the clone properly using the built-in git-svn within SourceTree (UI or command line). I repeatedly ran into issues on `-r 74752`, which perhaps was too large. Using an Ubuntu 14.04.2 LTS server and the git-svn package, I was able to run the commands listed below without issue.

* `git svn clone -r 73576:HEAD https://dotspatial.svn.codeplex.com/svn/Trunk`
    * _revision **73576** is when the most recent Trunk folder element was created_ 
    * this will link the subversion commits to the existing git commits (note this will take a very long time)
    * this will only track the trunk commits, none of the branches will be cloned
    * **NOTE**: append a folder name to clone into, or `.` to clone into the current folder. Otherwise the clone will appear in a folder named Trunk.
* `git svn fetch`
	* this will fetch any new subversion commits
* `git svn rebase`
    * this will fetch new subversion commits and rebase them onto the HEAD
    * **NOTE**: `git svn fetch` is not required for `git svn rebase`, however, you may fetch first then issue `git svn rebase -l` to only rebase already fetched subversion commits

## Development agreement
* Main namespace for core dlls - DotSpatial.*
* Main namespace for plugins - DotSpatial.Plugins.*
* Use AnyCPU target for projects always when it's possible.
* Plugins which works only in Windows, should have output folder "..\Windows Extensions\PluginFolder".
* Plugins which works in Windows and Mono, should have output folder "..\Plugins\PluginFolder".
* Every release must be labeled in TFS.
* Use branches when adding a lot of changes.
* Write unit-tests, when it is possible.
* Use resources for message strings. Use language-specific .resx files when needed to localize GUI.
